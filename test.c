#include <stdio.h>

#define ADD(a, b) (a + b)

int main(){
	int b, i;
	for(i = 0; i < 10; i++){
		b = ADD(1, 2);
		printf("%d\n", b);
	}
	return 0;
}
