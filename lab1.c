/* Lab1.c - Program to measure and analyze cache memory access time */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
#include <time.h>

/* @Function: Read Time Stamp Counter
 * @returns: 64-bit unsigned integer timestamp
 *
 */
uint64_t rdtsc(){
	unsigned int start, end;
	asm volatile("rdtsc"
					: "=a" (start), "=d" (end));
	return (uint64_t)end << 32 | start;
}

int main(int argc, char **argv){
		long int  retval, i, j, index = 0, array_size = 1000, k;
		uint64_t start_access, end_access, p, q, diff;
		unsigned long int *array;

		/* Help function from command-line */
		if(argc > 2 && strcmp(argv[1], "--help") == 0){
				printf("Tool to analyze access time for array elements.\n");
				return 0;
		}

		srand(time(NULL));
		/* Touch array of increasing elements in loop */
		for(i = 0; i < 20; i++){
				array = (unsigned long int*)malloc(sizeof(unsigned long int) * array_size);
				if(array == NULL){
						return -1;
				}
				for(j = 0; j < array_size; j++){
						array[j] = (rand() % array_size + 1);
				}
				/* rdtsc to get clock cycles */
				start_access = rdtsc();
				for(j = 0; j < array_size; j++){
						index = array[index];
				}
				end_access = rdtsc();
				diff += (end_access - start_access);
				diff /= (array_size);
				printf("%ld Total time for access = %"PRIu64"\t%ld\n",index,
								diff, array_size);
				free(array);
				array_size *= 2;
		}
	return 0;
}
