#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>

#define ADD(a, b) (a + b)
#define FADD(a, b) ADD(a, b) + ADD(a, b) + ADD(a, b) + ADD(a, b) + ADD(a, b)
#define TADD(a, b) FADD(a, b) + FADD(a, b) + FADD(a, b) + FADD(a, b) + FADD(a, b)
#define OADD(a, b) TADD(a, b) + TADD(a, b) + TADD(a, b) + TADD(a, b) + TADD(a, b)
#define SADD(a, b) OADD(a, b) + OADD(a, b) + OADD(a, b) + OADD(a, b) + OADD(a, b)
#define TSADD(a, b) SADD(a, b) + SADD(a, b) + SADD(a, b) + SADD(a, b) + SADD(a, b)
#define FFADD(a, b) TSADD(a, b) + TSADD(a, b) + TSADD(a, b) + TSADD(a, b) + TSADD(a, b)
#define FFFADD(a, b) FFADD(a, b) + FFADD(a, b) + FFADD(a, b) + FFADD(a, b) + FFADD(a, b)
#define FFFFADD(a, b) FFFADD(a, b) + FFFADD(a, b) + FFFADD(a, b) + FFFADD(a, b) + FFFADD(a, b)
#define FFFFFADD(a, b) FFFFADD(a, b) + FFFFADD(a, b) + FFFFADD(a, b) + FFFFADD(a, b) + FFFFADD(a, b)

uint64_t rdtsc()
{
	unsigned int start, end;
	asm volatile("rdtsc"
					: "=a" (start), "=d" (end));
	return (uint64_t)end << 32 | start;
}

int main(int argc, char **argv)
{
	int i, a, b;
	while(i < 1000){
		b = FFFFADD(i, i+1);
		printf("%d\n", b);
		i++;
	}
	return 0;
}
